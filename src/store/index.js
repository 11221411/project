import { createStore } from 'vuex'
import ukrTours from './modules/ukrTours'
import euTours from './modules/euTours'
import favorites from './helpers/favorites'
import auth from './modules/auth'

export default createStore({
    namespaced: true,
    modules: {
        ukrTours,
        euTours,
        favorites,
        auth,
    },
})

