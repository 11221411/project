import {createRouter, createWebHashHistory} from 'vue-router'

import HomePage from '@/views/HomePage.vue'
import ToursPage from '@/views/ToursPage.vue'
import UkraineToursPage from '@/components/UkraineToursPage.vue'
import EuropeToursPage from '@/components/EuropeToursPage.vue'
import LikedPage from '@/views/LikedPage.vue'
import ContactsPage from '@/views/ContactsPage.vue'
import  LoginPage  from "@/views/LoginPage.vue";

const routes = [
    {path: '/', component: HomePage, name: 'home',},
    {
        path: '/tours', 
        component: ToursPage,}, 
    {
        path: '/ukraine',
        component: UkraineToursPage,
        props: { category: 'ukraine' }
    },
    {
        path: '/europe',
        component: EuropeToursPage,
        props: { category: 'europe' }
    },
    {path: '/liked', component: LikedPage, name: 'booking'},
    {path: '/contacts', component: ContactsPage, name: 'contacts'},
    {path: '/login', component: LoginPage, name:'login'},
]

const router = createRouter({
    history: createWebHashHistory(),
    routes,
})
export default router