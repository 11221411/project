import { addDoc, collection, deleteDoc, doc, getFirestore } from 'firebase/firestore/lite';

const favoritesModule = {
  namespaced: true,
  state: () => ({
    favoritesList: [],
  }),
  getters: {
    getFavoritesList: (state) => state.favoritesList,
  },
  mutations: {
    addToFavoritesMutation(state, tour) {
      state.favoritesList.push(tour);
    },
    removeFromFavoritesMutation(state, itemId) {
      state.favoritesList = state.favoritesList.filter((item) => item.id !== itemId);
    },
    setFavoritesList(state, favoritesList) {
      state.favoritesList = favoritesList;
    },
  },
  actions: {
    async addToFavorites({ commit }, tour) {
      try {
        const db = getFirestore();
        const favoritesCollection = collection(db, 'favorites'); 
        const newDocRef = await addDoc(favoritesCollection, tour);

        commit('addToFavoritesMutation', { ...tour, id: newDocRef.id });
      } catch (error) {
        console.error('Error adding to favorites:', error);
      }
    },
    async removeFromFavorites({ commit }, itemId) {
      try {
        const db = getFirestore();
        const favoritesDocRef = doc(db, 'favorites', itemId);
        await deleteDoc(favoritesDocRef);

        // Commit mutation
        commit('removeFromFavoritesMutation', itemId);
      } catch (error) {
        console.error('Error removing from favorites:', error);
      }
    },
  },
};

export default favoritesModule;
