import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore/lite'
import {getAuth} from'firebase/auth';

const firebaseConfig = {
  apiKey: "AIzaSyAj-ULRBS_6FbmMvWYc8FmNMJgWF33_9tE",
  authDomain: "my-project-2688b.firebaseapp.com",
  projectId: "my-project-2688b",
  storageBucket: "my-project-2688b.appspot.com",
  messagingSenderId: "608944942485",
  appId: "1:608944942485:web:bd0fc3da2031b167bd1a9c"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app)
export const auth = getAuth(app);
export default db